### Maven Package

Maven can be packaged by mvn deploy or gradle.  This example is mvn deploy.

Maven Packaging documentation: https://docs.gitlab.com/ee/user/packages/maven_repository/index.html

Notice that the Maven Package registry can be written to using the CI_JOB_TOKEN - which is convenient as a Personal Access Token (PAT) is not needed.
